# calc-thingy

This is a calculator CLI.

This program was developed for the Software Engineering Process 2 (SE-3800)
course at Milwaukee School of Engineering.


## Dependencies

- Java 8
- JUnit 4
- Maven


## Build

Clone this repository, and run tests:
``` sh
git clone git@bitbucket.org:mattmahn/calc-thingy.git
cd calc-thingy
mvn test
```


## Team Members

- Matthew Mahnke
- Conrad Ratschan
