package calculator.operations;

import calculator.exceptions.InvalidOperationException;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * @author Matthew Mahnke
 */
public class OpFactoryTest {

    double[] happyTerms = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
    Operation op;

    @Test
    public void testHappyFactory() throws Exception {
        op = OpFactory.createOperation("add", happyTerms);
        assertThat(op, instanceOf(Add.class));

        op = OpFactory.createOperation("sub", happyTerms);
        assertThat(op, instanceOf(Subtract.class));

        op = OpFactory.createOperation("mul", happyTerms);
        assertThat(op, instanceOf(Multiply.class));

        op = OpFactory.createOperation("div", happyTerms);
        assertThat(op, instanceOf(Division.class));

        op = OpFactory.createOperation("fib", happyTerms);
        assertThat(op, instanceOf(Fibonacci.class));

        op = OpFactory.createOperation("clear", happyTerms);
        assertThat(op, instanceOf(Clear.class));

    }

    @Test(expected = InvalidOperationException.class)
    public void testInvalidCommands() throws Exception {
        op = OpFactory.createOperation("addition", happyTerms);
        op = OpFactory.createOperation("addends", happyTerms);

        op = OpFactory.createOperation("subtract", happyTerms);
        op = OpFactory.createOperation("subtraction", happyTerms);
        op = OpFactory.createOperation("diff", happyTerms);

        op = OpFactory.createOperation("multiplicate", happyTerms);
        op = OpFactory.createOperation("multiplicands", happyTerms);
        op = OpFactory.createOperation("multiplication", happyTerms);

        op = OpFactory.createOperation("divide", happyTerms);
        op = OpFactory.createOperation("division", happyTerms);
        op = OpFactory.createOperation("dividends", happyTerms);

        op = OpFactory.createOperation("fibonacci", happyTerms);
        op = OpFactory.createOperation("lucas", happyTerms);

        op = OpFactory.createOperation("claer", happyTerms);
        op = OpFactory.createOperation("free", happyTerms);
        op = OpFactory.createOperation("empty", happyTerms);
    }
}
