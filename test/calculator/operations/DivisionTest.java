package calculator.operations;

import calculator.exceptions.DivByZeroException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Conrad Ratschan
 */
public class DivisionTest {

    Operation op;
    List<Double> results;

    double[] normalVals1 = {102,2,3};
    double[] normalVals2 = {3, 3};
    double[] normalVals3 = {1.5, 2};
    double[] normalVals4 = {500, 2, 2};
    double[] normalVals5 = {0, 5};

    double normalResult1 = 17;
    double normalResult2 = 1;
    double normalResult3 = 0.75;
    double normalResult4 = 125;
    double normalResult5 = 0;

    @Test
    public void testNormalUse(){
        op = new Division(normalVals1);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult1, results.get(0).doubleValue(), 0.001);

        op = new Division(normalVals2);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult2, results.get(0).doubleValue(), 0.001);

        op = new Division(normalVals3);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult3, results.get(0).doubleValue(), 0.001);

        op = new Division(normalVals4);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult4, results.get(0).doubleValue(), 0.001);

        op = new Division(normalVals5);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult5, results.get(0).doubleValue(), 0.001);
    }


    double[] invalidValues1 = {};

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testInvalid(){
        op = new Division(invalidValues1);
        results = op.performOp();
    }


    double[][] invalidValues2 = {{1, 0},
                                {7, 5, 0},
                                {0, 0},
                                {40, 0}};

    @Test
    public void testDivByZero(){
        for(double[] invalidSet: invalidValues2){
            try{
                op = new Division(invalidSet);
                results = op.performOp();
                fail();
            }catch (DivByZeroException dze){
                //Do nothing
            }
        }
    }
}
