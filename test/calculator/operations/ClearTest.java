package calculator.operations;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Matthew Mahnke
 */
public class ClearTest {

    Clear clear;
    double[] arr;

    @Test
    public void testPerformOp() throws Exception {
        arr = new double[0];
        clear = new Clear();
        assertNull(clear.performOp());

        arr = new double[]{1};
        clear = new Clear(arr);
        assertNull(clear.performOp());

        arr = new double[]{1, 2, 6, 4, 5, 3};
        clear = new Clear(arr);
        assertNull(clear.performOp());

        arr = new double[2048];
        clear = new Clear(arr);
        assertNull(clear.performOp());
    }
}
