package calculator.operations;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Conrad Ratschan
 */
public class MultiplyTest {

    Operation op;
    List<Double> results;

    double[] normalVals1 = {2,3,5};
    double[] normalVals2 = {1.1, 2};
    double[] normalVals3 = {512, 0, 284};
    double[] normalVals4 = {0, 0, 0};

    double normalResult1 = 30;
    double normalResult2 = 2.2;
    double normalResult3 = 0;
    double normalResult4 = 0;

    @Test
    public void testNormalUse(){
        op = new Multiply(normalVals1);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult1, results.get(0).doubleValue(), 0.001);

        op = new Multiply(normalVals2);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult2, results.get(0).doubleValue(), 0.001);

        op = new Multiply(normalVals3);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult3, results.get(0).doubleValue(), 0.001);

        op = new Multiply(normalVals4);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult4, results.get(0).doubleValue(), 0.001);
    }


    double[] invalidValues1 = {};

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testInvalid(){
        op = new Multiply(invalidValues1);
        results = op.performOp();
    }

}
