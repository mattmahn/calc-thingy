package calculator.operations;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Conrad Ratschan
 */
public class AddTest {
    Operation op;
    List<Double> results;

    double[] normalVals1 = {2,3,5};
    double[] normalVals2 = {1.1, 2.2, 3.3};
    double[] normalVals3 = {1};
    double[] normalVals4 = {0, 0, 0};

    double normalResult1 = 10;
    double normalResult2 = 6.6;
    double normalResult3 = 1;
    double normalResult4 = 0;

    @Test
    public void testNormalUse(){
        op = new Add(normalVals1);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult1, results.get(0).doubleValue(), 0.001);

        op = new Add(normalVals2);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult2, results.get(0).doubleValue(), 0.001);

        op = new Add(normalVals3);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult3, results.get(0).doubleValue(), 0.001);

        op = new Add(normalVals4);
        results = op.performOp();
        assertEquals(1, results.size());
        assertEquals(normalResult4, results.get(0).doubleValue(), 0.001);
    }

}
