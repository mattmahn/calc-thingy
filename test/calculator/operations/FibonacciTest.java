package calculator.operations;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author Matthew Mahnke
 */
public class FibonacciTest {

    Fibonacci fib;

    double happy1 = 1;
    double[] happy1Result = new double[]{0};
    double happy2 = 2;
    double[] happy2Result = new double[]{0, 1};
    double happy3 = 6;
    double[] happy3Result = new double[]{0, 1, 1, 2, 3, 5};
    double happy4 = 11;
    double[] happy4Result = new double[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55};
    double happy5 = 48;
    double[] happy5Result = new double[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55,
        89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711,
        28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269,
        2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169,
        63245986, 102334155, 165580141, 267914296, 433494437, 701408733,
        1134903170, 1836311903, 2971215073L};

    @Test
    public void testNormalOperation() {
        fib = new Fibonacci(0);
        assertTrue(assertArrayContains(new double[]{}, fib.performOp()));

        fib = new Fibonacci(happy1);
        assertTrue(assertArrayContains(happy1Result, fib.performOp()));

        fib = new Fibonacci(happy2);
        assertTrue(assertArrayContains(happy2Result, fib.performOp()));

        fib = new Fibonacci(happy3);
        assertTrue(assertArrayContains(happy3Result, fib.performOp()));

        fib = new Fibonacci(happy4);
        assertTrue(assertArrayContains(happy4Result, fib.performOp()));

        fib = new Fibonacci(happy5);
        assertTrue(assertArrayContains(happy5Result, fib.performOp()));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testNoTerms() {
        double[] terms = {};
        fib = new Fibonacci(terms);
        fib.performOp();
    }

    private boolean assertArrayContains(double[] expected, List<Double> actual) {
        if (expected.length == actual.size()) {
            for (int i = 0; i < expected.length; i++) {
                if (!isClose(expected[i], actual.get(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean isClose(double v1, double v2) {
        final double DELTA = 1E-6;
        return (v1 > v2 - DELTA) && (v1 < v2 + DELTA);
    }
}
