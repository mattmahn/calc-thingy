package calculator;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.*;

/**
 * @author Matthew Mahnke
 */
public class CalculatorTest {

    Calculator calc;
    String res;

    @Test
    public void testUserStoryAcceptanceCriteria() {
        calc = new Calculator();
        res = calc.handleOperation("add 2 3 5");
        assertThat(res, containsString("10"));

        calc = new Calculator();
        res = calc.handleOperation("sub 2 3 5");
        assertThat(res, containsString("-6"));

        calc = new Calculator();
        res = calc.handleOperation("mul 2 3 5");
        assertThat(res, containsString("30"));

        calc = new Calculator();
        res = calc.handleOperation("div 102 2 3");
        assertThat(res, containsString("17"));

        calc = new Calculator();
        res = calc.handleOperation("div 102 2 0 3");
        assertThat(res, containsString("ERR: div by 0"));

        //////////////////////////////////////////////////////////////////////
        // NOTE: The order of handleOperation calls matters here
        calc = new Calculator();
        calc.handleOperation("add 1 2 3");
        calc.handleOperation("sub 85 5 2");

        res = calc.handleOperation("add !1 !2");
        assertThat(res, containsString("84"));

        res = calc.handleOperation("clear");
        res = calc.handleOperation("hist");
        assertThat(res, startsWith("Full History"));
        assertFalse(res.matches("\\d+"));
        //////////////////////////////////////////////////////////////////////

        calc = new Calculator();
        res = calc.handleOperation("fib 7");
        assertThat(res, containsString("0.0 1.0 1.0 2.0 3.0 5.0 8.0"));

        calc = new Calculator();
        calc.handleOperation("add 1 2 3");
        res = calc.handleOperation("hist");
        assertEquals("Full History: \nOperation 1: ADD 1.0 2.0 3.0 \tResults: 6.0 \n", res);
    }

    @Test
    public void testErrors() {
        // InvalidOperationException
        try {
            calc = new Calculator();
            res = calc.handleOperation(null);
            fail("expected a null pointer exception");
        } catch (NullPointerException npe) {
            // expected, why expect anything but a NPE when passing null to a method
        }

        calc = new Calculator();
        res = calc.handleOperation("");
        assertThat(res, containsString("operation is invalid"));

        calc = new Calculator();
        res = calc.handleOperation("sven");
        assertThat(res, containsString("operation is invalid"));

        // NumberFormatException
        calc = new Calculator();
        res = calc.handleOperation("add 1f2 5&5 5%6");
        assertThat(res, containsString("terms entered were invalid"));

        calc = new Calculator();
        res = calc.handleOperation("div \t  ! ");
        assertThat(res, containsString("terms entered were invalid"));

        // IndexOutOfBounds Exception
        calc = new Calculator();
        res = calc.handleOperation("mul !1 !7");
        assertThat(res, containsString("history does not contain the term requested"));
    }
}
