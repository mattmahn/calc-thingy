package calculator;

import calculator.exceptions.DivByZeroException;
import calculator.exceptions.InvalidOperationException;
import calculator.operations.OpFactory;
import calculator.operations.Operation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Conrad Ratschan
 */
public class Calculator {

    private List<Operation> resultsHistory;

    public Calculator(){
        resultsHistory = new LinkedList<>();
    }

    public String handleOperation(String input){
        String[] terms = input.split(" ");
        String retVal = "";
        if(terms.length > 0) {
            if(terms[0].equals("hist")){
                retVal = buildFullHistoryString();
            }else {
                try {
                    Operation currentOp = OpFactory.createOperation(terms[0], parseTerms(terms));
                    List<Double> results = currentOp.performOp();
                    if (results == null) {
                        resultsHistory.clear();
                        retVal = "History of operations have been cleared\n";
                    } else {
                        retVal = buildResultsString(results);
                        resultsHistory.add(0, currentOp);
                    }

                } catch (InvalidOperationException e) {
                    retVal = "\nThat operation is invalid";
                } catch (NumberFormatException nfe) {
                    retVal = "\nThe terms entered were invalid";
                } catch (DivByZeroException dze) {
                    retVal = "\nERR: div by 0";
                } catch (ArrayIndexOutOfBoundsException aiobe){
                    retVal = "\nThere were an invalid number of terms entered";
                } catch (IndexOutOfBoundsException iobe){
                    retVal = "\nThe history does not contain the term requested";
                }
            }
        }
        return retVal;
    }

    private double[] parseTerms(String[] strTerms){
        List<Double> temp = new ArrayList<>();
        for(int i = 1; i < strTerms.length; i++){
            if(strTerms[i].charAt(0) == '!'){
                if(strTerms[i].length() < 2){
                    throw new NumberFormatException();
                }else{
                    String strIndex = strTerms[i].substring(1, strTerms[i].length());
                    int index = Integer.parseInt(strIndex);
                    temp.addAll(resultsHistory.get(index - 1).performOp());
                }
            }else{
                temp.add(Double.parseDouble(strTerms[i]));
            }
        }

        double[] terms = new double[temp.size()];
        for(int i = 0; i < temp.size(); i++){
            terms[i] = temp.get(i).doubleValue();
        }
        return terms;
    }

    private String buildResultsString(List<Double> results){
        StringBuilder sb = new StringBuilder();
        sb.append("Results: ");
        for(Double x: results){
            sb.append(x + " ");
        }
        sb.append("\n");
        return sb.toString();
    }

    private String buildFullHistoryString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Full History: \n");
        for (int i = 0; i < resultsHistory.size(); i++) {
            Operation item = resultsHistory.get(i);
            sb.append("Operation " + (i + 1) + ": " + item.toString() + "\t");
            sb.append(buildResultsString(item.performOp()));
        }
        return sb.toString();
    }
}
