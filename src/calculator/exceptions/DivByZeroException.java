package calculator.exceptions;

/**
 * @author Matthew Mahnke
 */
public class DivByZeroException extends ArithmeticException {
    public DivByZeroException() {
        super("attempted to divide by 0");
    }
}
