package calculator.exceptions;

/**
 * @author Conrad Ratschan
 */
public class InvalidOperationException extends Exception{
    public InvalidOperationException() {
        super("Attempted an invalid operation");
    }
}
