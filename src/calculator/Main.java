package calculator;

import java.util.Scanner;

/**
 * @author Conrad Ratschan
 */
public class Main {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        String input;
        boolean isDone = false;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to the calculator thingy");
        System.out.println("The following are valid commands:");
        System.out.println("  add, sub, mul, div, fib, clear, hist");
        System.out.println("You can specify a space-separated list of numbers to use for the command.");
        System.out.println("You may also use previous results using the the notation");
        System.out.println("  '!1' for the previous result, '!2' for the second-to-last result, etc.");
        System.out.println();
        while(!isDone){
            System.out.print(">>> ");
            input = scanner.nextLine().trim();
            if(!input.equalsIgnoreCase("quit")){
                System.out.println(calculator.handleOperation(input));
            } else {
                isDone = true;
            }
        }
    }
}
