package calculator.operations;

import java.util.List;

public class Clear extends Operation {

    public Clear(double... terms) {
        super(Operations.CLEAR, terms);
    }

    /**
     * Do nothing
     * @return null
     */
    @Override
    public List<Double> performOp() {
        return null;
    }
}
