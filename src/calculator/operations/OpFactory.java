package calculator.operations;

import calculator.exceptions.InvalidOperationException;

/**
 * @author Conrad Ratschan
 */
public class OpFactory {

    private OpFactory(){
    }

    public static Operation createOperation(String type, double[] terms) throws InvalidOperationException{
        type = type.toLowerCase();
        Operation operation = null;

        switch(type){
            case "div":
                operation = new Division(terms);
                break;
            case "mul":
                operation = new Multiply(terms);
                break;
            case "add":
                operation = new Add(terms);
                break;
            case "sub":
                operation = new Subtract(terms);
                break;
            case "fib":
                operation = new Fibonacci(terms);
                break;
            case "clear":
                operation = new Clear(terms);
                break;
            default:
                throw new InvalidOperationException();
        }

        return operation;
    }
}
