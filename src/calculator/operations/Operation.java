package calculator.operations;

import java.util.List;

/**
 * @author Matthew Mahnke
 */
public abstract class Operation {

    public enum Operations { ADD, MULT, SUB, DIV, FIB, CLEAR }

    protected Operations name;
    protected double[] terms;

    protected Operation(Operations name, double... terms) {
        this.name = name;
        this.terms = terms;
    }

    public abstract List<Double> performOp();

	/**
     * Returns a string of the format <code>command_name
     * space_separated_list_of_terms</code>
     *
     * @return string representation of this operation
     * @author Conrad Ratschan
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(name.toString() + " ");
        for(double term: terms){
            sb.append(term + " ");
        }
        return sb.toString();
    }
}
