package calculator.operations;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Conrad Ratschan
 */
public class Fibonacci extends Operation {

    public Fibonacci(double... terms) {
        super(Operations.FIB, terms);
    }

    @Override
    public List<Double> performOp() {
        List<Double> result = new ArrayList<>();

        long end = (long)terms[0];
        long a = 0;
        long b = 1;

        for(long i = 0; i < end; i++){
            result.add(a + 0.0);
            long temp = b;
            b = a + b;
            a = temp;
        }
        return result;
    }
}
