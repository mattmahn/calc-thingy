package calculator.operations;

import calculator.exceptions.DivByZeroException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;

/**
 * @author Matthew Mahnke
 */
public class Division extends Operation {

    public Division(double... terms) {
        super(Operations.DIV, terms);
    }

    @Override
    public List<Double> performOp() {
        if (Arrays.stream(terms).skip(1).anyMatch(val -> val == 0)) {
            throw new DivByZeroException();
        }

        List<Double> result = new ArrayList<>();
        result.add(terms[0]);
        for (int i = 1; i < terms.length; i++) {
            double prevResult = result.get(0);
            result.clear();
            result.add(0, prevResult / terms[i]);
        }
        return result;
    }
}
