package calculator.operations;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Matthew Mahnke
 */
public class Multiply extends Operation {

    public Multiply(double... terms) {
        super(Operations.MULT, terms);
    }

    @Override
    public List<Double> performOp() {
        List<Double> result = new ArrayList<>();
        result.add(terms[0]);
        for (int i = 1; i < terms.length; i++) {
            double prevResult = result.get(0);
            result.clear();
            result.add(0, prevResult * terms[i]);
        }
        return result;
    }
}
