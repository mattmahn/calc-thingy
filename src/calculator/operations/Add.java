package calculator.operations;

import java.util.Arrays;
import java.util.List;

/**
 * @author Matthew Mahnke
 */
public class Add extends Operation {

    public Add(double... terms) {
        super(Operations.ADD, terms);
    }

    @Override
    public List<Double> performOp() {
        return Arrays.asList(Arrays.stream(terms).sum());
    }
}
